# @joshuaavalon/knex-utils

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Utilities for Knex.

## Installation

```
npm i -D @joshuaavalon/knex-utils
```

[license]: https://gitlab.com/joshua-avalon/knex-utils/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshua-avalon/knex-utils/pipelines
[pipelines_badge]: https://gitlab.com/joshua-avalon/knex-utils/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@joshuaavalon/knex-utils
[npm_badge]: https://img.shields.io/npm/v/@joshuaavalon/knex-utils/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
