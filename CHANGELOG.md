# 1.0.0 (2019-08-01)


### Bug Fixes

* Fix CI clean up test in deploy ([24ef2f9](https://gitlab.com/joshua-avalon/knex-utils/commit/24ef2f9))
* Fix package version ([4e19c89](https://gitlab.com/joshua-avalon/knex-utils/commit/4e19c89))


### Features

* Initial Commit ([d5d77db](https://gitlab.com/joshua-avalon/knex-utils/commit/d5d77db))
