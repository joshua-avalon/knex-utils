export abstract class Table<Model, Entity> {
  public constructor() {
    this.mapModel = this.mapModel.bind(this);
    this.mapEntity = this.mapEntity.bind(this);
  }

  public abstract readonly name: string;
  public abstract readonly idColumn: keyof Model & keyof Entity;
  public abstract mapModel(model: Model): Entity;
  public abstract mapModel(model: Partial<Model>): Partial<Entity>;
  public abstract mapEntity(entity: Entity): Model;
  public abstract mapEntity(entity: Partial<Entity>): Partial<Model>;
}

export abstract class SimpleTable<T> extends Table<T, T> {
  public mapModel(model: T): T;
  public mapModel(model: Partial<T>): Partial<T>;
  public mapModel(model: T | Partial<T>): T | Partial<T> {
    return model;
  }

  public mapEntity(entity: T): T;
  public mapEntity(entity: Partial<T>): Partial<T>;
  public mapEntity(entity: T | Partial<T>): T | Partial<T> {
    return entity;
  }
}
