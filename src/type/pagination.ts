export type Pagination<Model> = {
  models: Model;
  from: number;
  to: number;
  total: number;
  limit: number;
  current: number;
};
