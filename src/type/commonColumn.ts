export type CommonColumn<Model, Entity> = keyof Model & keyof Entity;
