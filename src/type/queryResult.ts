export type QueryResult<Model> = {
  models: Model[];
};
