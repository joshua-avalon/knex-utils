import { Table } from "./table";

export type IdColumn<
  Model,
  Entity,
  EntityTable extends Table<Model, Entity>
> = EntityTable["idColumn"];
