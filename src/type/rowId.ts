import { Table } from "./table";
import { IdColumn } from "./idColumn";

export type RowId<
  Model,
  Entity,
  EntityTable extends Table<Model, Entity>
> = Entity[IdColumn<Model, Entity, EntityTable>];
