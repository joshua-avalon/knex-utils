import { QueryBuilder } from "knex";

import { count, isPositiveInteger } from "@/utils";

export type OffsetEdge<Node> = {
  node: Node;
};

export type OffsetPageInfo = {
  current: number;
  limit: number;
  totalPage: number;
  total: number;
};

export type OffsetPagination<Node> = {
  edges: OffsetEdge<Node>[];
  pageInfo: OffsetPageInfo;
};

export type OffsetOptions = {
  limit: number;
  page: number;
};

async function getEdges<Node>(
  query: QueryBuilder,
  offset: number,
  limit: number
): Promise<OffsetEdge<Node>[]> {
  const nodes: Node[] = await query
    .clone()
    .offset(offset)
    .limit(limit);
  return nodes.map(node => ({ node }));
}

export async function offsetPaginate<Node>(
  query: QueryBuilder,
  options: OffsetOptions
): Promise<OffsetPagination<Node>> {
  const { limit, page } = options;
  if (!isPositiveInteger(limit)) {
    throw new Error("limit must be positive integer!");
  }
  if (!isPositiveInteger(page)) {
    throw new Error("page must be positive integer!");
  }
  const offset = (options.page - 1) * limit;
  const [edges, total] = await Promise.all([
    getEdges<Node>(query, offset, limit),
    count(query)
  ]);
  const pageInfo = {
    total,
    limit,
    current: page,
    totalPage: Math.ceil(total / limit)
  };
  return { edges, pageInfo };
}
