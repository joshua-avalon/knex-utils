import _ from "lodash";

import { createKnex } from "@/__tests__/utils";
import { offsetPaginate } from "../offset";

type Data = {
  id: string;
  value: string;
};

describe("pagination", () => {
  describe("offset", () => {
    describe("cursorPaginate", () => {
      const knex = createKnex();
      beforeAll(async () => {
        await knex.schema.createTable("test", table => {
          table.increments("id").primary();
          table.string("value");
        });
        const values = _.range(1, 5).map(i => ({
          value: `${i}`
        }));
        await knex("test").insert(values);
      });
      afterAll(() => {
        knex.destroy();
      });

      test("valid", async () => {
        const query = knex.select().from("test");
        const result = await offsetPaginate<Data>(query.clone(), {
          limit: 3,
          page: 1
        });
        expect(result.edges.length).toBe(3);
        expect(result.pageInfo.limit).toBe(3);
        expect(result.pageInfo.current).toBe(1);
        expect(result.pageInfo.total).toBe(4);
        expect(result.pageInfo.totalPage).toBe(2);
      });

      test("invalid options", async () => {
        const query = knex.select().from("test");
        const result = await offsetPaginate<Data>(query.clone(), {
          limit: 3,
          page: 5
        });
        expect(result.edges.length).toBe(0);
        expect(result.pageInfo.limit).toBe(3);
        expect(result.pageInfo.current).toBe(5);
        expect(result.pageInfo.total).toBe(4);
        expect(result.pageInfo.totalPage).toBe(2);

        const promise = offsetPaginate<Data>(query.clone(), {
          limit: -1,
          page: 1
        });
        await expect(promise).rejects.toThrow();
        const promise2 = offsetPaginate<Data>(query.clone(), {
          limit: 1,
          page: -1
        });
        await expect(promise2).rejects.toThrow();
      });
    });
  });
});
