import _ from "lodash";

import { createDataTable, createKnex } from "@/__tests__/utils";
import { cursorPaginate } from "../cursor";

type Data = {
  id: string;
  value: string;
};

describe("pagination", () => {
  describe("cursor", () => {
    describe("cursorPaginate", () => {
      const knex = createKnex();
      beforeAll(async () => {
        await createDataTable(knex, "test");
        const values = _.range(1, 5).map(i => ({
          value: `${i}`
        }));
        await knex("test").insert(values);
      });
      afterAll(() => {
        knex.destroy();
      });

      test("valid", async () => {
        const query = knex.select().from("test");
        const result = await cursorPaginate<Data>(query.clone(), { limit: 3 });
        expect(result.edges.length).toBe(3);
        expect(result.pageInfo.endCursor).toBeDefined();
        expect(result.pageInfo.hasNext).toBe(true);

        const result2 = await cursorPaginate<Data>(query.clone(), {
          limit: 3,
          cursor: result.pageInfo.endCursor
        });
        expect(result2.edges.length).toBe(1);
        expect(result2.pageInfo.endCursor).toBeDefined();
        expect(result2.pageInfo.hasNext).toBe(false);

        const query2 = knex
          .select()
          .from("test")
          .where({ value: -1 });
        const result3 = await cursorPaginate<Data>(query2.clone(), {
          limit: 3
        });
        expect(result3.edges.length).toBe(0);
        expect(result3.pageInfo.endCursor).toBeUndefined();
        expect(result3.pageInfo.hasNext).toBe(false);
      });

      test("invalid options", async () => {
        const query = knex.select().from("test");
        const promise = cursorPaginate<Data>(query.clone(), { limit: -1 });
        await expect(promise).rejects.toThrow();
        const result = await cursorPaginate<Data>(query.clone(), {
          limit: 3,
          cursor: "!"
        });
        expect(result.edges.length).toBe(0);
        const promise2 = cursorPaginate<Data>(query.clone(), {
          limit: 3,
          cursorColumn: "_id"
        });
        await expect(promise2).rejects.toThrow();
      });
    });
  });
});
