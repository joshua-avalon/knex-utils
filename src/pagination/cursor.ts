import _ from "lodash";
import { QueryBuilder } from "knex";

import { count, isPositiveInteger } from "@/utils";

export type CursorEdge<Node> = {
  node: Node;
  /**
   * Cursor of current edge. Should only be used as cursor.
   */
  cursor: string;
};

export type CursorPageInfo = {
  /**
   * Cursor of the last edge. `undefined` if there is not edges.
   */
  endCursor?: string;
  /**
   * true if there are more edges after `endCursor`.
   */
  hasNext: boolean;
};

export type CursorPagination<Node> = {
  edges: CursorEdge<Node>[];
  pageInfo: CursorPageInfo;
};

export type CursorOptions = {
  /**
   * Max numbers of result to return.
   *
   * It must be a positive integer.
   */
  limit: number;
  /**
   * Cursor to start the query from. It is exclusive.
   */
  cursor?: string;
  /**
   * Column name of the primary key. It must be included in the query.
   * It should be a comparable and not nullable column.
   *
   * Result will be sorted by this column.
   *
   * Default to `id`.
   */
  cursorColumn?: string;
};

function decode(cursor: string): string {
  return Buffer.from(cursor, "base64").toString();
}

function encode(value: any): string {
  return Buffer.from(value.toString()).toString("base64");
}

export async function cursorPaginate<Node>(
  query: QueryBuilder,
  options: CursorOptions
): Promise<CursorPagination<Node>> {
  const { limit, cursor, cursorColumn = "id" } = options;
  if (!isPositiveInteger(limit)) {
    throw new Error("limit must be positive integer!");
  }
  let resultQuery = query.clone();
  if (cursor) {
    const id = decode(cursor);
    resultQuery = resultQuery.andWhere(cursorColumn, ">", id);
  }
  const nodes: Node[] = await resultQuery.orderBy(cursorColumn).limit(limit);

  const edges = nodes.map(node => ({
    node,
    cursor: encode(node[cursorColumn])
  }));
  const last = _.last(edges);
  const endCursor = last ? last.cursor : undefined;
  let hasNext = false;
  if (last) {
    const id = last.node[cursorColumn];
    const nextQuery = query
      .clone()
      .andWhere(cursorColumn, ">", id)
      .orderBy(cursorColumn);
    const total = await count(nextQuery);
    hasNext = total > 0;
  }
  const pageInfo = { limit, endCursor, hasNext };
  return { edges, pageInfo };
}
