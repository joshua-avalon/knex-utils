import _ from "lodash";

import { createDataTable, createKnex } from "./utils";
import { deleteModels, insertModels, updateModels } from "../func";
import { SimpleTable } from "../type";

type Data = {
  id: number;
  value: string;
};

class InsertTable extends SimpleTable<Data> {
  public readonly name = "insert";
  public readonly idColumn = "id";
}

class DeleteTable extends SimpleTable<Data> {
  public readonly name = "delete";
  public readonly idColumn = "id";
}

class UpdateTable extends SimpleTable<Data> {
  public readonly name = "update";
  public readonly idColumn = "id";
}

describe("func", () => {
  const knex = createKnex();
  const insertTable = new InsertTable();
  const deleteTable = new DeleteTable();
  const updateTable = new UpdateTable();
  beforeAll(async () => {
    await Promise.all([
      createDataTable(knex, insertTable.name),
      createDataTable(knex, deleteTable.name),
      createDataTable(knex, updateTable.name)
    ]);
    const values = _.range(1, 5).map(i => ({
      value: `${i}`
    }));
    await Promise.all([
      knex(insertTable.name).insert(values),
      knex(deleteTable.name).insert(values),
      knex(updateTable.name).insert(values)
    ]);
  });
  afterAll(() => {
    knex.destroy();
  });

  test("insert", async () => {
    const result: Data[] = await knex(insertTable.name).select();
    expect(result.length).toBe(4);
    await insertModels(knex, insertTable, [{ value: "5" }]);
    const result2: Data[] = await knex(insertTable.name).select();
    expect(result2.length).toBe(5);
  });
  test("delete", async () => {
    const result: Data[] = await knex(deleteTable.name).select();
    expect(result.length).toBe(4);
    await deleteModels(knex, deleteTable, [1]);
    const result2: Data[] = await knex(deleteTable.name).select();
    expect(result2.length).toBe(3);
  });
  test("update", async () => {
    const value = "value";
    await updateModels(knex, updateTable, [{ id: 1, value }]);
    const result: Data[] = await knex(updateTable.name)
      .select()
      .where({ id: 1 });
    expect(result.length).toBe(1);
    expect(result[0].value).toBe(value);
  });
});
