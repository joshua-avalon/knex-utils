import Knex from "knex";

export function createKnex<TRecord = any, TResult = unknown[]>(): Knex<
  TRecord,
  TResult
> {
  return Knex<TRecord, TResult>({
    client: "sqlite3",
    connection: ":memory:",
    useNullAsDefault: true
  });
}

export async function createDataTable(knex: Knex, name: string): Promise<void> {
  await knex.schema.createTable(name, table => {
    table.increments("id").primary();
    table.string("value");
  });
}
