import _ from "lodash";
import Knex from "knex";

import { RowId, Table } from "./type";

export async function insertModels<Model, Entity>(
  knex: Knex,
  table: Table<Model, Entity>,
  models: Partial<Model>[]
): Promise<void> {
  await knex(table.name).insert(models.map(table.mapModel));
}

export async function deleteModels<
  Model,
  Entity,
  EntityTable extends Table<Model, Entity>
>(
  knex: Knex,
  table: Table<Model, Entity>,
  ids: RowId<Model, Entity, EntityTable>[]
): Promise<void> {
  await knex(table.name)
    .whereIn(table.idColumn, ids)
    .delete();
}

export async function updateModels<Model, Entity>(
  knex: Knex,
  table: Table<Model, Entity>,
  models: Partial<Model>[]
): Promise<void> {
  await knex.transaction(async trx => {
    const promises = models.map(async model => {
      const entity = table.mapModel(model);
      if (!(table.idColumn in entity)) {
        throw new Error(`Missing ${table.idColumn}. Skipping updating.`);
      }
      const id = entity[table.idColumn];
      const other = _.omit(entity, table.idColumn);
      return knex(table.name)
        .transacting(trx)
        .where(table.idColumn, id)
        .update(other);
    });
    return Promise.all(promises);
  });
}
