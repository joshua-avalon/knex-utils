import { QueryInterface } from "knex";

import _ from "lodash";

export function isPositiveInteger(value: any): value is number {
  return _.isInteger(value) && value > 0;
}
/**
 * Get total numbers of row of a query.
 *
 * `query` should not use `__count` as column name. Also, `query` should not use `GROUP BY`.
 */
export async function count(query: QueryInterface): Promise<number> {
  const result: { __count: number }[] = await query
    .clone()
    .count({ __count: "*" });
  return result.length > 0 ? result[0].__count : 0;
}
